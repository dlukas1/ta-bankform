var currentTab = 0;
var tabs = document.getElementsByClassName("tab");

function showCurrentTab(n){
    currentTab = n;
    tabs[currentTab].style.display = "block"; //need to display intro
    //adjust button text
     switch(currentTab) {
        //if intro tab - button text = "Start" and prev is hidden
        case 0:
            document.getElementById("btnPrev").style.display = "none";
            document.getElementById("btnNext").innerHTML = "Start";
            break;
        //first tab with inputs don't need button back
        case 1:
            document.getElementById("btnPrev").style.display = "none";
            document.getElementById("btnNext").innerHTML = "Next >>";
            break;
        //if last question - button text = "Submit" and next is hidden
        case (tabs.length-2):
            document.getElementById("btnNext").innerHTML = "Submit";
            break;
        //result tab don't need any buttons
        case (tabs.length-1):
            document.getElementById("btnPrev").style.display = "none";
            document.getElementById("btnNext").style.display = "none";
            submit();
            break;
        //in any other case leave buttons labels unchanged
        default:
            document.getElementById("btnNext").innerHTML = "Next >>";
            document.getElementById("btnPrev").style.display = "block";
            document.getElementById("btnPrev").innerHTML = "<< Previous";
    }
}

//switchTab will receive +-1 to switch pages next or back
function switchTab(n){
    
    if (n == 1 && !validateTab()){ 
        return false;
    }
        
    currentTab += n;
    //hide all tabs
    for (let i = 0; i < tabs.length; i++) {
        tabs[i].style.display = "none";           
    }
    //hide warning message
    document.getElementById("alert").style.display = "none";
    document.getElementById("alert").innerHTML = "";
    //show current and update button labels
    showCurrentTab(currentTab)
}

function submit() {
    //let all the variables be local scoped
    var texts = document.getElementsByName("text-input");
    var polls = document.getElementsByClassName("poll");
    var country = document.getElementById("countries").value;
    var checkedValues = document.getElementsByClassName("feature");
    var comment = document.getElementById("comment").value;
    var personal = "";
    var checkedOptions = "";
    var selected;
   
    for (let i = 0; i < texts.length; i++) {
       personal += texts[i].value + " ";       
    }
    for (let i = 0; i < polls.length; i++) {
        if(polls[i].checked)
            selected = polls[i].value
    }
    for (let i = 0; i < checkedValues.length; i++) {
        if(checkedValues[i].checked){
            checkedOptions += checkedValues[i].value + " "
        }
    }
    //Compose result
    var result = 
        "Contacts: " +  personal + "<br/>" + 
        "Card type: " + selected + " card<br/>"+ 
        "Country: " + country + "<br/>" +
        "Features: " + checkedOptions + "<br/>" +
        "Comment: " + comment + "<br/>";
    //Print to result tab
    document.getElementById("result").innerHTML = result;
    document.getElementById("alert").style.display = "block";
    document.getElementById("alert").style.backgroundColor = "green";
    document.getElementById("alert").innerHTML = "Thank you! Your form submitted successfully";

}