/*
Script to validate each tab at a time.
Because on each tab inputs are different - different test cases
should be used.
*/

function validateTab(){
var tabs = document.getElementsByClassName("tab");
var isValid = true;
    
    switch (currentTab) {
        //on first tab check text inputs
        case 1:          
            var inputs = tabs[currentTab].getElementsByTagName("input");
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].value == "") {
                //inputs[i].className += " invalid"; can show exact empty row 
                isValid = false;
            }           
        }
            if(!isValid){
                document.getElementById("alert").style.display = "block";
                document.getElementById("alert").innerHTML += "Please fill all fields<br/>";
            }
            if (!validateEmail()) {
                isValid = false;
            } 
            
            break;   

        //on 2nd tab check if radio button is selected
        case 2:
            isValid = false;
            var polls = document.getElementsByClassName("poll");
            for (let i = 0; i < polls.length; i++) {
                if(polls[i].checked)
                    isValid = true;
            }
            if(!isValid){
                document.getElementById("alert").style.display = "block";
                document.getElementById("alert").innerHTML += "Please select card type<br/>";
            }
            break;
        
        //on 3rd tab check if selectbox value not empty
        case 3:
            var country = document.getElementById("countries").value;
            if (country == "") {
                isValid = false;
            }
            if(!isValid){
                document.getElementById("alert").style.display = "block";
                document.getElementById("alert").innerHTML += "Please select your country<br/>";
            }
            break;

        //on 4th page check that at least 1 is checked
        case 4:
        var checkedValues = document.getElementsByClassName("feature");
        var numberOptionsChecked = 0;
        //count checked options
        for (let i = 0; i < checkedValues.length; i++) {
                if(checkedValues[i].checked){
                numberOptionsChecked++;
                }
            }
        if (numberOptionsChecked == 0) {
            document.getElementById("alert").style.display = "block";
            document.getElementById("alert").innerHTML += "Please select at least one option<br/>";
            isValid = false;
            }           
            break;

        //on 5th tab check if textarea is not empty
        case 5:
        var comment = document.getElementById("comment").value;
            if (comment == null || comment == "") {
                isValid = false;
            }
            if(!isValid){
                document.getElementById("alert").style.display = "block";
                document.getElementById("alert").innerHTML += "Please leave short feedback<br/>";
            }
            break;

        default:
            break;
    }   
    return isValid;
}

function validateEmail(){
    var validEmailFormat = false;
    var email = document.getElementById("email");
    if (email.value != "") {
        var re = /\S+@\S+/;
        if(re.test(email.value)){
            validEmailFormat = true;
        } else {
            isValid = false;
            document.getElementById("alert").style.display = "block";
            document.getElementById("alert").innerHTML = "Please enter valid email<br/>";
        }
    }
    return validEmailFormat;
}

